# Mapa de Colombia

Mapa administrativo de Colombia
El proyecto tiene como objetivo crear una herramienta propia para la visualización de datos asociados con la división administrativa de Colombia, como por ejemplo datos estadísticos del DANE. El proyecto consta de dos componentes. El primero es un mapa en vectores (SVG) que contiene la división administrativa del país a nivel de departamentos y municipios. Las cinco principales ciudades del país, Bogotá, Medellín, Cali, Barranquilla y Bucaramanga, están divididas adicionalmente en localidades y comunas según corresponda. Cada departamento, municipio, localidad y comuna está representada mediante un vector y una etiqueta que lo identifica por el nombre de la división administrativa. El segundo componente es un programa escrito en processing (JAVA) con cinco funciones 
- leer el archivo de vectores.
- presentar en la pantalla toda la división administrativa del país hasta el nivel de localidad/comuna 
- presentar en la pantalla un departamento escogido con su división administrativa 
- presentar en la pantalla una de las principales cinco ciudades con su división administrativa 
- exportar el mapa presentado en pantalla en formato PDF

# Autor
Ricardo Cedeño Montaña
Universidad de Antioquia
Facultad de Comunicaciones y Filología
