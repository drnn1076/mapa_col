/**
 * Load and Display a *.svg shape. 
 * Map of Colombia by Ricardo Cedeño Montaña 
 * 
 *
    Ricardo Cedeño Montaña
    Universidad de Antioquia
    Facultad de Comunicaciones
    CC-SA-BY 2019

    update 27.3.2019
    uptade 26.5.2021
*/


/*
1.  create a shape object for the entire map, 
    one for departamento and one for municipios
2.  create a shape object for a municipio
*/

import processing.svg.*;//this library exports to svg

PShape Colombia;
PShape Departamentos, Departamento, Municipios, Municipio;
PShape [] shMun;

// text files to store the name departments and 
// their municipios
PrintWriter output, output1;

/*
1.  create an xml variable to read the svg map as an xml file 
    in order to extract all labels and ids.
2.  create lists of departamentos, municipios and their ids 
    extracted from the svg as xml
*/

XML xmlCol; 
String [] listDptos, listMun;
String [] listDepLabel, listDepId, listDepMunLabel, 
          listMunLabel, listMunId, listDepMunId, 
          listStyle, listClass, capitals, pathNodes;
int [] svgX, svgY, svgM, svgN;


//position, width, height, scale of the map, and scale factor for departamentos
int xPos = 0, yPos=0, svgW, svgH;
float mapScale = 1;
int scaleFac = 3; 

void setup() {
  size(2948, 4000);
  background(255);
  output = createWriter("departamentos");
  output1 = createWriter("municipios");
  
  // The SVG file has to exist in the data directory
  // for the sketch to load it properly
  
  //Read the SVG as shape
  Colombia = loadShape("mapa_col.svg");
  svgW = int(Colombia.getWidth());
  svgH = int(Colombia.getHeight());
  
  //read the SVG as an XML file
  //extract the name of all municipios and the departmentos
  //they belong to and organize the names in arrays
  //Print the values into separate text files
  
  xmlCol = loadXML("mapa_col.svg"); 
  
  XML[] xmlDep = xmlCol.getChildren("g/path"); //reads the node called Departamentos
  listDepId = new String[xmlDep.length];
  
  for (int i = 0; i < xmlDep.length; i++) {
    listDepId[i] = xmlDep[i].getString("id");
    output.println(listDepId[i]);//write departamentos
    output.flush(); // write the file 
  }
  output.close(); // close the file
  
  XML[] xmlMun = xmlCol.getChildren("g/g/path");//read the node called Municipios
  
  listMunLabel = new String[xmlMun.length];
  listMunId = new String[xmlMun.length];
  listDepMunLabel = new String[xmlMun.length];
  listClass = new String[xmlMun.length];
  capitals = new String[xmlDep.length-1];//-1 bc bogota doesn't have a capital
  listDepMunId = new String[xmlDep.length-1];//-1 bc bogota doesn't have a capital
  svgX = new int[xmlDep.length-1];//-1 bc bogota doesn't have a capital
  svgY = new int[xmlDep.length-1];//-1 bc bogota doesn't have a capital
  svgM = new int[xmlDep.length-1];//-1 bc bogota doesn't have a capital
  svgN = new int[xmlDep.length-1];//-1 bc bogota doesn't have a capital
  //println(listMunId.length);
  
  
  int add = 0;//counter
  for (int i = 0; i < xmlMun.length; i++) {
  
    listDepMunLabel[i] = xmlMun[i].getParent().getString("inkscape:label");

    listMunLabel[i] = xmlMun[i].getString("inkscape:label");
    listClass[i] = xmlMun[i].getString("class");
    
    listMunId[i] = listMunLabel[i]+" "
                   +listClass[i]+" "
                   +listDepMunLabel[i];
    
    if(listClass[i].contains("capital")){// extract the path id for each capital
      capitals[add] = xmlMun[i].getString("id");
      listDepMunId[add] = xmlMun[i].getParent().getString("id"); //<>//
      //extract x and y positions of each group and map them to the sketch size.
      svgX[add] = int(xmlMun[i].getParent().getString("xPos"));
      svgY[add] = int(xmlMun[i].getParent().getString("yPos"));
      svgM[add] = int(map(svgX[add], 0, svgW, 0, width))*scaleFac;
      svgN[add] = int(map(svgY[add], 0, svgH, 0, height))*scaleFac;
      
      add++;
    }
  
    output1.println(listMunLabel[i]);//write only the name of municipios
    output1.flush();
  }
  
  saveStrings("municipios detalle", listMunId);//write details of each municipio
  saveStrings("municipios id", capitals);//id path
  output1.close();
  
  
  /** functions to draw either deps or muns
    *  comment and uncomment to draw what you need
    *  Default option is municipios
  */
  
  //drawDepartamentos();
  drawMunicipios();
  //drawCapitals(capitals);
  //drawOneDpto(i, svgM[i], svgN[i], scaleFac);
  //drawComunas();
  
  //for(int i=0;i<32;i++){
    //beginRecord(SVG, "col_dpto_"+i+".svg");
    /*export each departmento to svg
    you lose all attributes of the original map e.g. names of municipios.
    */
    //drawOneDpto(i, svgM[i], svgN[i], scaleFac);
  //}
  
} 

/* =========== Functions =========== */

void drawDepartamentos(){
  // go through all departamentos node from the SVG
  // Draw nodes on the sketch
  Departamentos = Colombia.getChild("Departamentos");
  
    listDptos = new String[Departamentos.getChildren().length];
    for (int i=0; i<Departamentos.getChildren().length; i++){
        listDptos[i] = Departamentos.getChild(i).getName();
        
        shape(Departamentos.getChild(listDptos[i]),xPos, yPos,
      width, height);
     };
  //export the map to PNG
  save("col_departamentos.png");
}

void drawMunicipios(){
  // go through all municipios node from the SVG
  // Draw all nodes on the sketch
  Municipios = Colombia.getChild("Municipios");
  
  listMun = new String[Municipios.getChildren().length];
  for (int i =0; i<Municipios.getChildren().length; i++){
    listMun[i] = Municipios.getChild(i).getName();
    
    shape(Municipios.getChild(listMun[i]),xPos, yPos,
      width, height);
  }
  //export the map to PNG
  save("col_municipios.png");
}

void drawCapitals(String[] listCapitals){//gets path ids of each capital as an array
  // go through all municipios from the SVG
  // Draw them on the sketch
  Departamentos = Colombia.getChild("Departamentos");
  Municipios = Colombia.getChild("Municipios");
  listDptos = new String[Departamentos.getChildren().length];
  listMun = new String[Municipios.getChildren().length];
  //xValue and yValue are hardcoded bc svg translates groups g/ from the origin
  float xValue = 241.0025, yValue = 360.192;
  float xPos = map(xValue, 0, svgW, 0, width);
  float yPos = map(yValue, 0, svgH, 0, height);
  
  int add = 0;//counter
  for (int i=0; i<Departamentos.getChildren().length; i++){
       listDptos[i] = Departamentos.getChild(i).getName();
       
      //draw the shape of the department 
      shape(Departamentos.getChild(listDptos[i]),0, 0,
      width, height);
      listMun[i] = Municipios.getChild(i).getName();
      
      // draw the capital of the department
      if(add<listCapitals.length){
         shape(Municipios.getChild(listMun[i]).getChild(listCapitals[add]),
         xPos, yPos,width, height);
       }
       add++; // counter + 1
      }
      
  //export the map to PNG
  save("col_capitales.png");
  
}
  
  
void drawOneDpto(int dpto, int svgM, int svgN, int scale){//

  // go through all municipios nodes from the SVG
  // Draw on dpto the sketch (see: var dpto)
  background(255);
  Departamento = Colombia.getChild("Municipios");
  pushMatrix();
  translate(-svgM, -svgN);
  
  
  shape(Departamento.getChild(listDepMunId[dpto]), xPos, yPos,
      width*scale, height*scale);
  popMatrix();
  
  textSize(72);
  fill(0);
  text(listDepId[dpto], width/2 , height-200);
  
      
  //export the map to PNG
  save("col_dpto_"+dpto+".png");
  endRecord();//finished exporting the svg file
}  


//listMun[i] = Municipios.getChild(i).getName();
  //int numMun = Municipios.getChild(i).getChildCount();
  
  //int add = 0;//counter
  //for (int i=0; i<Municipios.getChildren().length; i++){
  //     listMun[i] = Municipios.getChild(i).getName();
  //     int numMun = Municipios.getChild(dpto).getChildCount();
  //     //println(numMun);
      
  //    // draw the capital of the department
  //    if(add<listCapitals.length){
  //       shape(Municipios.getChild(dpto).getChild(listCapitals[add]),
  //       xPos, yPos,width*mapScale, height*mapScale);
  //     }
  //     add++; // counter + 1
  //    }

  
  //for(int j=0; j<numMun; j++){
         //println(Municipios.getChild(i).getChild(j).getName());
         
         //for(int k=0; k<listCapitals.length-1;k++){
         
           //if(Municipios.getChild(i).getChild(j).getName()==listCapitals[k]){
           //  println(Municipios.getChild(i).getChild(listCapitals[k]));
           //}
           //println(listCapitals[k]);
           //add++;
           //}
           
         //}
  
  //for (int i =0; i<Municipios.getChildren().length; i++){
  //  listMun[i] = Municipios.getChild(i).getName();
    
  //    if(listMun[i]==listCapitals[add]){
  //      shape(Municipios.getChild(listMun[i]),xPos, yPos,
  //       width*mapScale, height*mapScale);    
  //      add++;
  //    }
    
  //}
  
  //depsId = Municipios.getChild(1);
  ////capitals = Colombia.getChild("Municipios").getChild().length;
  //listMun = new String[Municipios.getChildren().length-1];
  
  //for(int i=0;i<listMun.length;i++){
  //   depsId = Municipios.getChild(i);
  //   println(depsId);
  //}
  //capitals = new String[listCapitals.length];
  //println(listMun.length);
  //for (int i =0; i<Municipios.getChildren().length; i++){
  //   for (int j=0; j<listCapitals.length; j++){
  //     //println(listCapitals[j]);
  //     //println(listMun[i]);
  //     if(listMun[i]==listCapitals[j]){
  //       println(listMun[i]);    
  //     }
     
     
      
  //  //shape(Municipios.getChild(listMun[i]),xPos, yPos,
  //  //  width*mapScale, height*mapScale);
  //  }
      //xmlMun[i].getString("inkscape:label");
  //}
